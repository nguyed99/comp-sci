#include "QuadratureRule.h"


QuadratureRule::QuadratureRule(const std::vector<double>& w, const std::vector<double>& x)
    : weights(w), points(x) {}

    // the integrate method takes a callable function f and integrates it over the domain R
double QuadratureRule::integrate(const std::function<double(double)>& f, double a, double b) {
    double integral = 0;
    double stepSize = (b-a)/2;

    // the integration is a weighted sum of function values at the specified points
    for (size_t i = 0; i < weights.size(); i++){
        integral += weights[i] * f(points[i]);
    }
    
    integral *= stepSize;

    return integral;
}