// QuadratureRule.h
#ifndef QUADRATURERULE_H
#define QUADRATURERULE_H

#include <vector>
#include <functional>

class QuadratureRule {
private:
    std::vector<double> weights;
    std::vector<double> points;

public:
    QuadratureRule(const std::vector<double>& w, const std::vector<double>& x);
    double integrate(const std::function<double(double)>& f, double a, double b);
};

#endif // QUADRATURERULE_H
