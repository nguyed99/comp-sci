#include "QuadratureRule.h"
#include <iostream>

int main() {
    std::function<double(double)> func = [](double x) { return x * x; };

    std::vector<double> weights = {1.0/3.0, 4.0/3.0, 1.0/3.0};
    std::vector<double> points = {-1,0,1};

    QuadratureRule quadrature(weights, points);
    double a = -1;
    double b = 1;

    double result = quadrature.integrate(func,a,b);

    std::cout << "Approximate integral of f(x) = x^2 over [-1, 1] is: " << result << std::endl;

    return 0;
}

