import numpy as np

class polynomial:
    
    def __init__(self, coeff):
        self.coeff = coeff
        self.deg = len(coeff) - 1
        self._diff_coeff = np.zeros(self.deg + 1)
        self._diff_coeff[:-1] = np.arange(1, self.deg + 1) * self.coeff[1:]
        
    def __call__(self, x):
        if isinstance(x, float):
            x = np.array([x])
            X = self._evaluate_powers(x)
            return (self.coeff @ X)[0]
        else:
            X = self._evaluate_powers(x)
            return self.coeff @ X
    
    def diff_p(self, x):
        X = self._evaluate_powers(x)
        
        return self._diff_coeff @ X
        
    def _evaluate_powers(self, x):
        X = np.zeros((self.deg + 1, len(x)))
        for ii in range(self.deg + 1):
            X[ii, :] = x**ii
        
        return X

class polynomial_new(polynomial):
    
    def __init__(self, coeff):
        # This method calls the constructor of the parent class:
        super(polynomial_new, self).__init__(coeff)
        # Overwrite array of derivative coefficients, they are replaced by
        # a matrix of coefficients for all (relevant) derivatives:
        self._diff_coeff = self._eval_diff_coeff()
        
    
    # Overwrite derivative method:
    def diff_p(self, x, order=0):
        
        if order > self.deg:
            return np.zeros(x.shape)
        else:
            # Evaluate all powers of x at all data sites:
            X = self._evaluate_powers(x)

            return self._diff_coeff[order, :] @ X
    
    # New function to generate derivative coefficients:
    def _eval_diff_coeff(self):
        # Compute all derivative coefficients:
        diff_coeff = np.zeros((self.deg + 1, self.deg + 1))
        diff_coeff[0, :] = self.coeff
        for ii in range(1, self.deg + 1):
            if ii == 1:
                upper_ind = None
            else:
                upper_ind = -ii + 1
            diff_coeff[ii, :-ii] = np.arange(1, self.deg + 2 - ii) * diff_coeff[ii-1, 1:upper_ind]
        
        return diff_coeff