import numpy as np

class QuadratureRule:

    def __init__(self, x, w):
        self.x = x
        self.w = w
        self._n = self.x.shape[0]

    def integrate(self, f):
        # Evaluate f on all grid points:
        fx = np.array([f(self.x[i]) for i in range(self._n)])

        return np.dot(self.w, fx)
    


class LinearTransformed(QuadratureRule):

    def integrate(self, f, T, dT):
        # Evaluate f on transformed grid points:
        fx = np.array([f(T(self.x[i])) for i in range(self._n)])
        # Transform weights:
        wT = np.array([self.w[i] * dT for i in range(self._n)])

        return np.dot(wT, fx)


class CompositeQuadrature:

    def __init__(self, a, b, n, x, w):
        """ Composite quadrature on interval [a, b] using n sub-
         intervals, and a quadrature rule on reference interval [-1, 1].
        
        Parameters:
        a, b:   lower and upper bounds of integration domain
        n:      number of sub-intervals
        x:      grid points for reference quadrature rule
        w:      weights for reference quadrature rule
        """
        self.a = a
        self.b = b
        self.n = n
        # Create sub-division of [a, b]:
        self.h = (b - a) / n
        self.y = np.array([a + i * self.h for i in range(self.n+1)])
        # Mid-points of the sub-intervals:
        self.c = 0.5 * (self.y[:-1] + self.y[1:])
        # Create quadrature rule object:
        self.QR = LinearTransformed(x, w)

    def integrate(self, f):
        I = 0.0
        # Iterate over sub-intervals:
        for i in range(self.n):
            # Linear transformation:
            T = lambda x: 0.5 * self.h * x + self.c[i]
            I += self.QR.integrate(f, T, 0.5*self.h)
        
        return I