import numpy as np
import matplotlib.pyplot as plt

import quadrature_full

""" Settings: """
# Function to integrate:
f = lambda x: np.arctan(x)
# Anti-derivative:
F = lambda x: x * np.arctan(x) - 0.5 * np.log(1 + x**2)

# Interval:
a = -1.0
b = 3.0

# Array of grid sizes:
n_array = np.array([5, 10, 50, 100, 500, 1000, 5000, 10000])
# Reference quadrature:
x = np.array([-1.0, 0.0, 1.0])
w = np.array([1.0/3, 4.0/3, 1.0/3])

""" Demonstration: """
# Compute reference:
Iref = F(b) - F(a)
# Convert grid sizes to step sizes:
h_array = (b - a) / n_array
# Apply composite quadrature:
E = np.zeros(h_array.shape[0])
for i in range(h_array.shape[0]):
    n = n_array[i]
    QC = quadrature_full.CompositeQuadrature(a, b, n, x, w)
    I = QC.integrate(f)
    E[i] = np.abs(I - Iref)

""" Figure: """
plt.figure(figsize=(6, 4))
plt.plot(h_array, E, "o--", label="Error Simpson")
plt.plot(h_array, h_array**4, "ro--", label=r"$h^4$")
plt.xscale("log")
plt.yscale("log")
plt.xlabel(r"$h$")
plt.ylim([1e-16, 1e-2])
plt.legend(loc=2)
plt.show()

print(I, Iref)