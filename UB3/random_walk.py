import numpy as np
import random
import matplotlib.pyplot as plt

from run_rw2d import _update_figure

class RandomWalk2D:
    def __init__(self, dx: float, N: int, x0: np.ndarray, dt: float):
        """
        :param x0: vector of initial positions
        :param N: population size
        """
        assert x0.shape == (N,2), "x0 must have the shape (N,2)"
        self.positions = x0
        self.time = [0]
        self.dt = dt
        self.N = N
        self.dx = dx
    
    def simulate(self, K: int = 1):
        """
        param K: number of discrete steps
        """

        choices = [(-1, 0), (1, 0), (0, 1), (0, -1)]

        for _ in range(K):
            self.time.append(self.time[-1]+ self.dt)
            directions = np.array([random.choice(choices) for _ in range(self.N)])* self.dx
            self.positions += directions


### testing ###
N=100
K=20
dt=4e-2
dx=4e-1   
RW = RandomWalk2D(dx=dx, N=N, x0 = np.zeros((N,2)), dt=dt)

### visualisation ###

# Axes limits for figure:
xmin = -8.0
xmax = 8.0
X = RW.positions
fig = plt.figure(figsize=(6, 4))
fig = _update_figure(fig, X, 0.0, xmin, xmax)
for _ in range(1, K+1):
    # Update positions:
    RW.simulate()
    X = RW.positions
    print(f'{X=}')
    t = RW.time[-1]
    # Update figure:
    fig = _update_figure(fig, X, t, xmin, xmax)
    plt.pause(0.25)

plt.show()
